﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppleCatch.Audio.BGM
{
    public class BGMPlayer : MonoBehaviour
    {
        public static BGMPlayer instance;

        public AudioClip clip;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }

            else
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            GetComponent<AudioSource>().clip = clip;
            GetComponent<AudioSource>().Play();
        }
    }
}

