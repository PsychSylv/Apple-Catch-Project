﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using AppleCatch.Menu.Main;

namespace AppleCatch.Menu.Manager
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        public GameObject survivalCanvas;
        public GameObject timeTrialCanvas;
        public GameObject pauseCanvas;
        public GameObject gameOverCanvas;

        private int _isPause = 0;
        private int _canPause = 1;
        private int _gameMode;

        private void Awake()
        {
            instance = this;   
        }

        private void Start()
        {
            _gameMode = MainMenu.instance.GetGameMode();
            _canPause = 1;

            if(_gameMode == 0)
            {
                if(!survivalCanvas.activeInHierarchy)
                {
                    survivalCanvas.SetActive(true);

                    if(timeTrialCanvas.activeInHierarchy)
                    {
                        timeTrialCanvas.SetActive(false);
                    }
                }
            }

            else if (_gameMode == 1)
            {
                if (!timeTrialCanvas.activeInHierarchy)
                {
                    timeTrialCanvas.SetActive(true);

                    if (survivalCanvas.activeInHierarchy)
                    {
                        survivalCanvas.SetActive(false);
                    }
                }
            }
        }

        private void Update()
        {
            if(_canPause == 1)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (_isPause == 0)
                    {
                        pauseCanvas.SetActive(true);
                        Time.timeScale = 0f;
                        _isPause = 1;
                    }

                    else
                    {
                        ResumeUBEvent();
                    }
                }
            }
        }

        public void ResumeUBEvent()
        {
            pauseCanvas.SetActive(false);
            Time.timeScale = 1f;
            _isPause = 0;
        }

        public void ExitUBEvent()
        {
            ResumeUBEvent();
            SceneManager.LoadScene(0);
        }

        public void GameOver()
        {
            gameOverCanvas.SetActive(true);
            _canPause = 0;
            Time.timeScale = 0f;
        }
    }
}

