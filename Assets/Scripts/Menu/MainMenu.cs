﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace AppleCatch.Menu.Main
{
    public class MainMenu : MonoBehaviour
    {
        public static MainMenu instance;

        public Button survivalMode;
        public Button timeTrialMode;
        public Button exitGame;

        private int gameMode;

        private void Awake()
        {
            instance = this;
        }

        public void SurvivalUBEvent()
        {
            gameMode = 0;
            SceneManager.LoadScene(1);
        }

        public void TimeTrialUBEvent()
        {
            gameMode = 1;
            SceneManager.LoadScene(1);
        }

        public void ExitGameUBEvent()
        {
            Application.Quit();
        }

        public int GetGameMode()
        {
            return gameMode;
        }
    }
}
