﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AppleCatch.Object.Spawner
{
    public class ObjectSpawner : MonoBehaviour
    {
        public GameObject[] prefab;

        private float _minSpawnTime = 0.5f;
        private float _maxSpawnTime = 3f;

        private float _spawnTime = 1f;
        private float _spawnLocation;
        private int _spawnIndex;


        void Start()
        {
            StartCoroutine(Spawn(_spawnTime));
        }

        IEnumerator Spawn(float spawnTime)
        {
            yield return new WaitForSeconds(_spawnTime);

            _spawnLocation = Random.Range(Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x, Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x);
            _spawnIndex = Random.Range(0, prefab.Length);
            _spawnTime = Random.Range(_minSpawnTime, _maxSpawnTime);

            Instantiate(prefab[_spawnIndex], new Vector3(_spawnLocation, 6f, 0f), Quaternion.identity);

            StartCoroutine(Spawn(_spawnTime));
        }
    }
}

