﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppleCatch.Menu.Main;
using AppleCatch.Mode.Survival.Score;

namespace AppleCatch.Mode.Destoryer
{
    public class ObjectDestroyer : MonoBehaviour
    {
        private int _gameMode;
        void Start()
        {
            _gameMode = MainMenu.instance.GetGameMode();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(_gameMode == 0 && collision.CompareTag("Apple"))
            {
                SurvivalScore.instance.MinusHealth();
            }

            Destroy(collision.gameObject);
        }
    }
}

