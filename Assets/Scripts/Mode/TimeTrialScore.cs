﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AppleCatch.Menu.Manager;

namespace AppleCatch.Mode.TimeTrial.Score
{
    public class TimeTrialScore : MonoBehaviour
    {
        public static TimeTrialScore instance;
        public Text scoreText;
        public Text timeText;

        private int _score = 0;
        private float _time = 60;

        private void Awake()
        {
            instance = this;
        }

        private void FixedUpdate()
        {
            UpdateTime();
        }

        public void AddScore()
        {
            _score += 3;
            UpdateScoreText();
        }
        public void UpdateScoreText()
        {
            scoreText.text = "Score: " + _score.ToString();
        }

        public void UpdateTime()
        {
            _time -= Time.deltaTime;
            UpdateTimeText();

            if(_time <= 0)
            {
                GameManager.instance.GameOver();
            }
        }

        public void UpdateTimeText()
        {   
            timeText.text = "Time: " + ((int)_time).ToString();
        }

    }
}

