﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AppleCatch.Menu.Manager;

namespace AppleCatch.Mode.Survival.Score
{
    public class SurvivalScore : MonoBehaviour
    {
        public static SurvivalScore instance;
        public Text scoreText;
        public Text healthText;

        private int _score = 0;
        private int _health = 3;

        private void Awake()
        {
            instance = this;
        }

        public void AddScore()
        {
            _score += 3;
            UpdateScoreText();
        }

        public void UpdateScoreText()
        {
            scoreText.text = "Score: " + _score.ToString();
        }

        public void MinusHealth()
        {
            _health -= 1;
            UpdateHealthText();
            
            if(_health <= 0)
            {
                GameManager.instance.GameOver();
            }
        }

        public void UpdateHealthText()
        {
            healthText.text = _health.ToString();
        }
    }
}

