﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppleCatch.Unit.Basket.Controller
{
    public class BasketController : MonoBehaviour
    {
        public static BasketController instance;

        public float speed = 10f;
        public float stunTime = 3f;
        private float canMove = 1;

        private Rigidbody2D rb;

        private void Awake()
        {
            instance = this;   
        }

        private void Start()
        {
            rb = gameObject.GetComponent<Rigidbody2D>();            
        }

        void Update()
        {
            Move();
        }

        private void Move()
        {
            if(Input.GetKey(KeyCode.A) && canMove == 1)
            {
                rb.velocity = new Vector3(-(speed), 0, 0);
            }

            else if(Input.GetKey(KeyCode.D) && canMove == 1)
            {
                rb.velocity = new Vector3(speed, 0, 0);
            }

            else
            {
                rb.velocity = new Vector3(0, 0, 0);
            }
        }

        public void Stun()
        {
            StartCoroutine(StunDuration());
        }

        IEnumerator StunDuration()
        {
            canMove = 0;

            yield return new WaitForSeconds(stunTime);
            canMove = 1;
        }
    }
}