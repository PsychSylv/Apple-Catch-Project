﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppleCatch.Menu.Main;
using AppleCatch.Unit.Basket.Controller;
using AppleCatch.Mode.Survival.Score;
using AppleCatch.Mode.TimeTrial.Score;

namespace AppleCatch.Unit.Basket.Catcher
{
    public class BasketCatcher : MonoBehaviour
    {
        private int _gameMode;

        void Start()
        {
            _gameMode = MainMenu.instance.GetGameMode();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (_gameMode == 0)
            {
                if (collision.CompareTag("Apple"))
                {
                    SurvivalScore.instance.AddScore();
                    Destroy(collision.gameObject);
                }

                else
                {
                    SurvivalScore.instance.MinusHealth();
                    Destroy(collision.gameObject);
                }
            }

            else if(_gameMode == 1)
            {
                if (collision.CompareTag("Apple"))
                {
                    TimeTrialScore.instance.AddScore();
                    Destroy(collision.gameObject);
                }

                else
                {
                    BasketController.instance.Stun();
                    Destroy(collision.gameObject);
                }
            }
        }   
    }
}

